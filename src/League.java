import java.util.ArrayList;
import java.util.Collections;

public class League<T extends Team> {

    private String name;
    private ArrayList<T> league = new ArrayList<>();

    public League() {
    }

    public League(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<T> getLeague() {
        return league;
    }

    public void setLeague(ArrayList<T> league) {
        this.league = league;
    }

    public void addTeam(T team) {
        if (!league.contains(team)) {
            league.add(team);
            System.out.println("Team " + team + " was added in the league " + name);
        } else {
            System.out.println("Team is already in your league! ");
        }
    }

    public void showTeam() {
        Collections.sort(league);
        for (T t : league) {
            System.out.println("team" + t);
        }
    }

    @Override
    public String toString() {
        return "League{" +
                "name='" + name ;
    }
}
