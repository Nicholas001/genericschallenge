public class Main {

    public static void main(String[] args) {

        FootballPlayer footballPlayer01 = new FootballPlayer("Hagi");
        FootballPlayer footballPlayer02 = new FootballPlayer("Popescu");
        FootballPlayer footballPlayer03 = new FootballPlayer("Maradona");
        FootballPlayer footballPlayer04 = new FootballPlayer("Ilie");


        Team<FootballPlayer> team = new Team<>("Steaua");
        team.addPlayer(footballPlayer01);
        team.addPlayer(footballPlayer02);
        team.addPlayer(footballPlayer03);
        team.addPlayer(footballPlayer04);

        League<Team<FootballPlayer>> league = new League<>("LigaI");
        league.addTeam(team);

        BaseballPlayer baseballPlayer01 = new BaseballPlayer("Jon");
        BaseballPlayer baseballPlayer02 = new BaseballPlayer("Meny");
        BaseballPlayer baseballPlayer03 = new BaseballPlayer("Andy");
        BaseballPlayer baseballPlayer04 = new BaseballPlayer("Gabrial");

        Team<BaseballPlayer> team02 = new Team<>("UFC");
        team02.addPlayer(baseballPlayer01);
        team02.addPlayer(baseballPlayer02);
        team02.addPlayer(baseballPlayer03);
        team02.addPlayer(baseballPlayer04);

        League<Team<BaseballPlayer>> league2 = new League<>("LigaII");
        league2.addTeam(team02);
        league2.addTeam(team02);
        System.out.println();
        league.showTeam();
        System.out.println();
        league2.showTeam();

//        System.out.println();
//        League<Team<FootballPlayer>> league0 = new League<>();
//        league0.addTeam(team);
//        league0.addTeam(team02);
//        league0.showTeam();

    }


}
